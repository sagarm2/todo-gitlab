
//generate a new unique ID
export const uniqueID = ()=>{
    var uniq = '' + (new Date()).getTime();
    return uniq;
};

//toggle color
export const findColor = (color) =>{
    if(color==="gray"){
        return "black";
    }
    else{
        return "gray";
    }
};
