import './App.css';
import Heading from "./components/Heading"
import Todo  from "./components/Todo"
import { headingTitle } from './constants/heading.constants';

function App() {
  return (
    <div className="App">
      <Heading>{headingTitle}</Heading>
      <Todo />
    </div>
  );
}

export default App;
