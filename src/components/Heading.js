import "./Heading.css"

const Heading = (props) =>{
    return (
        <div className="heading-container">
          <h1 className = "heading" >{props.children}</h1>
        </div>
       
    );
}

export default Heading;