import { useMemo, useState } from "react";
import "./Todo.css"

import NewTask from "./NewTask";
import Tasks from "./Tasks";
import Options from "./Options"
import { FILTER_TYPES } from '../constants/filter.constant'
import { INITIAL_TODOS } from "../constants/todo.constant";

const Todo = () =>{ // TodoList

   const [list,setList]  = useState(INITIAL_TODOS);
   const [filterType, setFilterType]  = useState(FILTER_TYPES.ALL);
   
   const addNewTask = (t) =>{
      setList([...list,t]);
   }
   
   const deleteTask = (id) =>{
        const filteredTasks = list.filter((item)=>{
         return item.id !== id;
        });
        setList(filteredTasks);
   }

   const setCheck = (id)=>{
      setList(list.map(item => {
         if(item.id === id) return { ...item, done: !item.done }
         return item
      }));
   };

   const onSelect = (filterType) => {
      setFilterType(filterType);
   }

   const getFilteredList = useMemo(() => {
      switch(filterType) {
         case FILTER_TYPES.ALL:
            return list;
         case FILTER_TYPES.ACTIVE:
            return list.filter(item => !item.done);
         case FILTER_TYPES.COMPLETED:
            return list.filter(item => item.done);
         default:
            return list;
      }
   }, [filterType, list])

   return (
   <div className="todo-container">
      <NewTask addTask = {addNewTask}></NewTask>
      <Tasks list={getFilteredList} remoTask = {deleteTask} setCheck={setCheck}></Tasks>
      <Options list={list} onSelect={onSelect}></Options>
   </div>
   );
}

export default Todo;  