import "./Tasks.css";
import Task from "./Task.js";

const Tasks = (props) => {
  const removeTask = (id) => {
    props.remoTask(id);
  };
  const set = (index) => {
    props.setCheck(index);
  };
  return (
    <div className="tasks">
      {props.list.map((item, i) => (
        <Task // pass only todo
          key={item.id}
          id={item.id}
          task={item.task}
          done={item.done}
          removeTask={removeTask}
          set={set}
        ></Task>
      ))}
    </div>
  );
};

export default Tasks;
