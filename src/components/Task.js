import "./Task.css";

const Task = (props) => {
  const removeThisTask = (e) => {
    props.removeTask(props.id);
  };

  const onClick = () => {
    props.set(props.id);
  };

  console.log("render");
  return (
    <div className="task-container">

      <div className="round">
        <input
          checked={props.done}
          type="checkbox"
          className="checkbox-btn"
          onClick={onClick}
          id={props.id}
        ></input>
        <label for={props.id}></label>
      </div>

      <p className={`task ${props.done ? "checked" : ""}`}>
        {props.task}
      </p>
      
      <button className="cross-btn" onClick={removeThisTask}>
        ❌
      </button>
    </div>
  );
};

export default Task;
