import { FILTER_TYPES } from '../constants/filter.constant'
import "./Options.css";

const Options = ({onSelect, list})=>{

    const handleAll = () =>{
        onSelect(FILTER_TYPES.ALL);
    }
    
    const handleActive = () =>{
        onSelect(FILTER_TYPES.ACTIVE);
    }

    const handleCompleted = () =>{
        onSelect(FILTER_TYPES.COMPLETED);
    }
    return (
    <div className="options">
        <p className="links">
            <span className="noi">{list.length} Items left</span> 
            <a  className="link all" onClick={handleAll}>All</a> 
            <a className="link" onClick={handleActive}>Active</a> 
            <a className="link" onClick={handleCompleted}>Completed</a>
        </p>
    </div>
    );
}

export default Options;