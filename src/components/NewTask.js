import "./NewTask.css"
import down from "../images/down.png"
import { useState } from "react";
import { uniqueID,findColor } from "../helpers/new-task.helpers";

const NewTask = (props)=>{

    const [task,setTask] = useState('');
    const [color,setColor] = useState("gray");

    const onChangeTask =  (e)=> {
        setTask(e.target.value);
    }
    const  submitHandler = (e)=> {
        e.preventDefault();
        let uniq = uniqueID();
        const obj = { id:uniq, task, done:false };
        props.addTask(obj);
        setTask('');

    }
    
    const colorHandler = ()=> {
        let newColor = findColor(color);
        setColor(newColor);
       
    }
    return ( 
    <div className="new-task">
        <button className="down-btn"> <img src={down} alt="down-arrow" className="down"></img> </button>
        <form onSubmit = {submitHandler}>
        <input required className="task-input" type="text" placeholder="What needs to be done ?" onClick = {colorHandler} style={{color:color}} onChange={onChangeTask} value={task}></input>
        <button type="submit" className="submit-btn"></button>
        </form>
    </div>
    );
}

export default NewTask;